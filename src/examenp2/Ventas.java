package examenp2;

/**
 *
 * @author LGCD
 */
public class Ventas {
    private String codigo;
    private float cantidad;
    private int tipo;
    
    public Ventas(){
    this.codigo="";   
    this.cantidad=0.0f;
    this.tipo=0;
    }


    public Ventas(String codigo, float cantidad, int tipo) {
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.tipo = tipo;
    }
    
     public Ventas(Ventas otro) {
        this.codigo = otro.codigo;
        this.cantidad = otro.cantidad;
        this.tipo = otro.tipo;
    
}

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
    
    public float calcularCantidad(){
    float cantidadLitro=0.0f;
    cantidadLitro=this.cantidad;
    return cantidadLitro;
    }
    
    public float calcularPrecioLitro(){
    float precioLitro=0.0f;
     if(tipo==1){
    precioLitro=this.cantidad*24.50f;
    }
    if(tipo==2){
    precioLitro=this.cantidad*20.50f;
    }
    return precioLitro;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.cantidad*this.calcularPrecioLitro();
    return total;
    }
}
     
     
