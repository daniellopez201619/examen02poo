package examenp2;

/**
 *
 * @author LGCD
 */
public class ExamenP2 {

    public static void main(String[] args) {
       Ventas ventas = new Ventas();
       ventas.setCodigo("1");
       ventas.setCantidad(2.0f);
       ventas.setTipo(1);
       
       System.out.println("Ejemplo 1: ");
       System.out.println("Codigo de venta: "+ventas.getCodigo());
       System.out.println("Cantidad: "+ventas.getCantidad());
       System.out.println("Tipo de Gasolina: "+ventas.getTipo());
       System.out.println("Cantidad: "+ventas.calcularCantidad());
       System.out.println("Precio Por Litro: "+ventas.calcularPrecioLitro());
       System.out.println("Total: "+ventas.calcularTotal());
       
       System.out.println("---------------------------------------------------------------------");
       
       ventas.setCodigo("2");
       ventas.setCantidad(2.0f);
       ventas.setTipo(2);
       
       System.out.println("Ejemplo 2: ");
       System.out.println("Codigo de venta: "+ventas.getCodigo());
       System.out.println("Cantidad: "+ventas.getCantidad());
       System.out.println("Tipo de Gasolina: "+ventas.getTipo());
       System.out.println("Cantidad: "+ventas.calcularCantidad());
       System.out.println("Precio Por Litro: "+ventas.calcularPrecioLitro());
       System.out.println("Total: "+ventas.calcularTotal());
       
    }
    
}
